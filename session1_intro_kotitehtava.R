#' ---
#' title: "Kotitehtävä 1"
#' author: utu_tunnus
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session1_intro_kotitehtava.R)

#+ setup, include=FALSE
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))


#' # Ennakkotehtävät seuraavalle luennolle
#' 
#' ## Lue ja omaksu
#' 
#' - [Wickham, Hadley. 2014. ‘Tidy Data’. Journal of Statistical Software 59 (10). doi:10.18637/jss.v059.i10](http://vita.had.co.nz/papers/tidy-data.html)
#' - [Greg Wilson, Jennifer Bryan, Karen Cranston, Justin Kitzes, Lex Nederbragt, Tracy K. Teal (2016) Good Enough Practices in Scientific Computing](https://arxiv.org/pdf/1609.00037v1.pdf)
#' - [Grolemund & Wickham (2016) Rfor Data Science](http://r4ds.had.co.nz/) - luvut 1-11
#' - [TIE-02200 Ohjelmoinnin peruskurssi - Git ja versionhallinta](http://www.cs.tut.fi/~opersk/S2015/@wrapper.shtml?materiaalit/git)
#' 
#' ## Katso
#' 
#' - Parin viikon takaisin luento data-analyysin tulevaisuudesta [Keynote Session: Dr. Edward Tufte - The Future of Data Analysis](https://channel9.msdn.com/Events/Machine-Learning-and-Data-Sciences-Conference/Data-Science-Summit-2016/MSDSS11?utm_source=hackernewsletter&utm_medium=email&utm_term=watching) (ei liity Microsoftiin!)
#' 
#' ## Yritä asentaa ohjelmistot omalle koneellesi (Jos haluat saada toimivan ympäristön haltuusi kurssin jälkeen) 
#' 
#' - Ohjeet ovat täällä <http://courses.markuskainu.fi/utur2016/ohjelmistoymparisto.html> **ks. sivun alalaita kohta 2.**
#' - **Kysymykset asennukseen liittyen tänne: <https://utur2016.slack.com/messages/softat_omalle_koneell/>**



